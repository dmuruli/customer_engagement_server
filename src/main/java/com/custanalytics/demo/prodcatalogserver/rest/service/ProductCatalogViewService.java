package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.entity.CustomerMediaView;
import com.custanalytics.demo.prodcatalogserver.rest.resource.ProductCatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductCatalogViewService {


    @RequestMapping(value = "/getWeekProductCatalogViewsBySalesPerson", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getProductViewsBySalesPersonForWeek(Long salesPersonId) {

        ProductCatalogResponse response = new ProductCatalogResponse();

        return response;
    }

    @RequestMapping(value = "/getMonthProductCatalogViewsBySalesPerson", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getProductViewsBySalesPersonForMonth(Long salesPersonId) {

        ProductCatalogResponse response = new ProductCatalogResponse();

        return response;
    }

    @RequestMapping(value = "/getYearProductCatalogViewsBySalesPerson", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getProductViewsBySalesPersonForYear(Long salesPersonId) {

        ProductCatalogResponse response = new ProductCatalogResponse();

        return response;
    }

    @RequestMapping(value = "/getWeekProductCatalogViewsBySalesTerritory", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getProductViewsBySalesTerritory() {
        ProductCatalogResponse response = new ProductCatalogResponse();
        return response;
    }

    @RequestMapping(value = "/getMonthProductCatalogViewsBySalesTerritory", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getMonthProductViewsBySalesTerritory() {
        ProductCatalogResponse response = new ProductCatalogResponse();
        return response;
    }

    @RequestMapping(value = "/getYearProductCatalogViewsBySalesTerritory", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public ProductCatalogResponse getYearProductViewsBySalesTerritory() {
        ProductCatalogResponse response = new ProductCatalogResponse();
        return response;
    }

}
