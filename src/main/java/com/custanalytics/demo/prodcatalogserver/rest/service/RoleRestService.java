package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import com.custanalytics.demo.prodcatalogserver.rest.resource.Request;
import com.custanalytics.demo.prodcatalogserver.rest.resource.RoleResponse;
import com.custanalytics.demo.prodcatalogserver.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RoleRestService {
    @Autowired
    RoleService roleService;

    @RequestMapping(value ="/getRoles", method = RequestMethod.GET, params = {"userid", "requestorid"})
    public RoleResponse getRoles(Long userid, Long requestorid){
        RoleResponse roleResponse = new RoleResponse();
        List<AppRole> appRoles = new ArrayList<AppRole>();
        roleResponse.setEntity(appRoles);
        return roleResponse;
    }

    @RequestMapping(value ="/getAppRole", method = RequestMethod.GET, params = {"roleid", "requestorid"})
    public RoleResponse getRole(){
        RoleResponse roleResponse = new RoleResponse();
        System.out.println("Could not find a role");
        return roleResponse;
    }

    @RequestMapping(value ="/saveRole", method = RequestMethod.POST)
    public RoleResponse saveRole (@RequestBody Request request){
        AppRole newAppRole = (AppRole) request.getRequestObject();
        roleService.saveRole(-999L, newAppRole);
        RoleResponse roleResponse = new RoleResponse();
        roleResponse.setSuccess(true);
        return roleResponse;
    }

}
