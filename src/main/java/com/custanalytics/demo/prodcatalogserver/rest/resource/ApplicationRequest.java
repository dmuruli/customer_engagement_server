package com.custanalytics.demo.prodcatalogserver.rest.resource;

import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import com.custanalytics.demo.prodcatalogserver.entity.CustomerMediaView;
import com.custanalytics.demo.prodcatalogserver.entity.SalesTerritory;


public class ApplicationRequest {
    AppUser appUser;
    AppRole appRole;
    SalesTerritory salesTerritory;
    CustomerMediaView customerMediaView;
    Long requestorId;

    public Long getRequestorId() {
        return requestorId;
    }

    public void setRequestorId(Long requestorId) {
        this.requestorId = requestorId;
    }

    public AppUser getAppUser() {return appUser;}

    public void setAppUser(AppUser appUser) {this.appUser = appUser;}

    public AppRole getAppRole() {
        return appRole;
    }

    public void setAppRole(AppRole appRole) {
        this.appRole = appRole;
    }

    public SalesTerritory getSalesTerritory() {
        return salesTerritory;
    }

    public void setSalesTerritory(SalesTerritory salesTerritory) {
        this.salesTerritory = salesTerritory;
    }

    public CustomerMediaView getCustomerMediaView() {
        return customerMediaView;
    }

    public void setCustomerMediaView(CustomerMediaView customerMediaView) {this.customerMediaView = customerMediaView;}
}
