package com.custanalytics.demo.prodcatalogserver.rest.resource;

import com.custanalytics.demo.prodcatalogserver.entity.AppUser;

public class AppUserResponse extends BaseResponse {

    AppUser appUser;
    Object entity;
    Object data;
    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }
}
