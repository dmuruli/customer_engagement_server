package com.custanalytics.demo.prodcatalogserver.rest.resource;

/**
 * Created by were on 12/15/2016.
 */
public class BaseRequest {
    long requestorId;
    long requestTime;
    String ip;
    String securityToken;

    public long getRequestorId() {
        return requestorId;
    }

    public void setRequestorId(long requestorId) {
        this.requestorId = requestorId;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }
}
