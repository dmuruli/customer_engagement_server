package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.dao.UserRepository;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import com.custanalytics.demo.prodcatalogserver.rest.resource.AppUserResponse;
import com.custanalytics.demo.prodcatalogserver.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:9080")
public class LoginRestService {

    private static final Logger logger = LoggerFactory
            .getLogger(LoginRestService.class);

    @Autowired
    LoginService loginService;
    //@RequestMapping("/login")
    @RequestMapping(value ="/login", method = RequestMethod.GET, params = {"userid", "password"})
    public AppUserResponse login(String userid, String password){

        AppUser appUser = loginService.login(userid, password);
        logger.info("Logging user: " + userid +" with password: " + password);
        AppUserResponse response = new AppUserResponse();
        if (appUser==null){
            response.setSuccess(false);
            response.setErrorMessage("User/Password combination Not Found");
        }else
        {
            response.setSuccess(true);
            response.setAppUser(appUser);
        }
        return response;
    }

}
