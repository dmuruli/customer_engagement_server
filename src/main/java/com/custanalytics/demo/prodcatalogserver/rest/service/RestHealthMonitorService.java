package com.custanalytics.demo.prodcatalogserver.rest.service;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestHealthMonitorService {

    @RequestMapping("/status")
    public String status(){
        Date now = new Date();
        return "System is running as of " + now;
    }

}
