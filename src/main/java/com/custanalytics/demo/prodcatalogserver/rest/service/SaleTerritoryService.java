package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.dao.SalesTerritoryRepository;
import com.custanalytics.demo.prodcatalogserver.entity.SalesTerritory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SaleTerritoryService {

    @Autowired
    SalesTerritoryRepository salesTerritoryRepository;
    public List<SalesTerritory> getAllSalesTerritories(Long requestorId){
        List<SalesTerritory> salesTerritories =salesTerritoryRepository.findAll();
        return salesTerritories;
    }

    public SalesTerritory findSalesTerritory(Long salesTerritoryId) {
        SalesTerritory salesTerritory = salesTerritoryRepository.findOne(salesTerritoryId);
        return salesTerritory;
    }
}
