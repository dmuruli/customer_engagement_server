package com.custanalytics.demo.prodcatalogserver.rest.service;


import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import com.custanalytics.demo.prodcatalogserver.rest.resource.AppUserResponse;
import com.custanalytics.demo.prodcatalogserver.rest.resource.ApplicationRequest;
import com.custanalytics.demo.prodcatalogserver.rest.resource.Request;
import com.custanalytics.demo.prodcatalogserver.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:9080")
public class AppUserRestService  {
    @Autowired
    AppUserService appUserService;

    @RequestMapping(value = "/getAppUsers", method = RequestMethod.GET, params = {"requestorid"})
    public AppUserResponse getAppUsers(Long requestorId) {
        AppUserResponse appUserResponse = new AppUserResponse();
        List<AppUser> appUsers =appUserService.getAllUsers();
        appUserResponse.setEntity(appUsers);
        return appUserResponse;
    }
    @CrossOrigin(origins = "http://localhost:9080")
    @RequestMapping(value = "/findAppUser", method = RequestMethod.GET, params = {"requestorId","userId"})
    public AppUserResponse findAppUser(Long requestorId, Long userId) {
        AppUserResponse appUserResponse = new AppUserResponse();
        AppUser appUser =appUserService.findUser(userId);
        appUserResponse.setEntity(appUser);
        return appUserResponse;
    }

    @RequestMapping(value = "/createAppUser", method = RequestMethod.POST, params = {"requestorId", "roleId"})
    public AppUserResponse createAppUser(Long requestorId,Long roleId,@RequestBody ApplicationRequest request) {
        AppUserResponse appUserResponse = new AppUserResponse();
        AppUser newAppUser=request.getAppUser();
        AppUser createdUser =appUserService.createUser(newAppUser,roleId,requestorId);
        appUserResponse.setEntity(createdUser);
        return appUserResponse;
    }

    @RequestMapping(value = "/updateAppUser", method = RequestMethod.POST)
    public AppUserResponse updateAppUser(@RequestBody ApplicationRequest request) {
        AppUserResponse appUserResponse = new AppUserResponse();
        AppUser appUser = request.getAppUser();
        AppUser updatedAppUser =appUserService.updateUser(appUser,request.getRequestorId());
        appUserResponse.setEntity(appUser);
        return appUserResponse;
    }
}
