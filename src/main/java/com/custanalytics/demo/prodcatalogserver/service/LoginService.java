package com.custanalytics.demo.prodcatalogserver.service;

import com.custanalytics.demo.prodcatalogserver.dao.UserRepository;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    @Autowired
    UserRepository userRepository;
    public AppUser login(String userName, String password){
        AppUser appUser = null;
        appUser =userRepository.login(userName, password);
        return appUser;
    }

    public AppUser register(AppUser newAppUser){
        AppUser persistedAppUser = null;
        return persistedAppUser;
    }
}
