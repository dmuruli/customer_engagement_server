package com.custanalytics.demo.prodcatalogserver.service;


import com.custanalytics.demo.prodcatalogserver.dao.UserRepository;
import com.custanalytics.demo.prodcatalogserver.dao.com.custanalytics.demo.prodcatalogserver.RoleRepository;
import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service

public class AppUserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    public List<AppUser> getAllUsers(){
       return userRepository.findAll();
    }

    public AppUser findUser(Long userId){
        return userRepository.findOne(userId);
    }

    public AppUser createUser(AppUser appUser, Long requestorId){
        appUser.setCreated(new Date());
        if(validUserAuthority(requestorId)){
            appUser.setCreatedBy(requestorId);
        }
        return userRepository.save(appUser);

    }

    public AppUser createUser(AppUser appUser,Long roleId, Long requestorId){
        appUser.setCreated(new Date());
        if(validUserAuthority(requestorId)){
            appUser.setCreatedBy(requestorId);
        }
        AppRole role =roleRepository.findOne(roleId);
        appUser.setAppRole(role);
        return userRepository.saveAndFlush(appUser);
    }

    public AppUser updateUser(AppUser appUser, long requestorId) {
        appUser.setUpdated(new Date());
        if(validUserAuthority(requestorId)){
            appUser.setUpdatedBy(requestorId);
        }
        return userRepository.saveAndFlush(appUser);
    }

    boolean validUserAuthority(Long requestorId){
        boolean valid = false;
        AppUser requestor =findUser(requestorId);
        if(requestor!=null && requestor.getAppRole().getId()==AppRoleEnum.ADMIN.getRole()){
            valid=true;
        }
        return valid;
    }


}
