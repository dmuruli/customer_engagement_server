package com.custanalytics.demo.prodcatalogserver.service;

import com.custanalytics.demo.prodcatalogserver.dao.com.custanalytics.demo.prodcatalogserver.RoleRepository;
import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RoleService {
    @Autowired
    RoleRepository roleRepository;

    public AppRole getRole(long roleId, long requestorId){
        AppRole appRole = null;
        return appRole;
    }

    public AppRole saveRole(long requestorId, AppRole newAppRole){
        AppRole savedAppRole = roleRepository.saveAndFlush(newAppRole);
        return savedAppRole;
    }

    public AppRole findAppRole(Long roleId) {
        AppRole foundAppRole = roleRepository.getOne(roleId);
        return foundAppRole;
    }

    public List<AppRole> listAllActiveRoles() {
        List<AppRole> appRoles =roleRepository.findAllActiveRoles();
        return  appRoles;
    }

    public List<AppRole> listAllRoles(){
        List<AppRole> appRoles = roleRepository.findAll();
        return appRoles;
    }
}
