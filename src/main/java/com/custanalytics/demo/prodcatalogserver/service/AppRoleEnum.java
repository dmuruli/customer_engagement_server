package com.custanalytics.demo.prodcatalogserver.service;


public enum AppRoleEnum {
    ADMIN(1),
    SALESPERSON(2);

    private long role;

    AppRoleEnum(long inRole) {
        role =inRole;
    }

    public long getRole() {
        return role;
    }
}
