package com.custanalytics.demo.prodcatalogserver.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CUSTOMER_MEDIA_VIEW")
public class CustomerMediaView extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /*private Customer customer;
    private ProductCatalogMedia productCatalogMedia;
    private Date dateViewed;*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ProductCatalogMedia getProductCatalogMedia() {
        return productCatalogMedia;
    }

    public void setProductCatalogMedia(ProductCatalogMedia productCatalogMedia) {
        this.productCatalogMedia = productCatalogMedia;
    }

    public Date getDateViewed() {
        return dateViewed;
    }

    public void setDateViewed(Date dateViewed) {
        this.dateViewed = dateViewed;
    }*/
}
