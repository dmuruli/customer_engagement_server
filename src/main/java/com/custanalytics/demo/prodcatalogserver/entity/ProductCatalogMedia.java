package com.custanalytics.demo.prodcatalogserver.entity;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_CATALOG_MEDIA")
public class ProductCatalogMedia extends BaseEntity  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
