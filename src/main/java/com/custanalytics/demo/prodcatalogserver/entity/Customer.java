package com.custanalytics.demo.prodcatalogserver.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CUSTOMER")
public class Customer extends BaseEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
/*    private SalesTerritory salesTerritory;*/
    private Date dateViewed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

 /*   public SalesTerritory getSalesTerritory() {
        return salesTerritory;
    }

    public void setSalesTerritory(SalesTerritory salesTerritory) {
        this.salesTerritory = salesTerritory;
    }*/

    public Date getDateViewed() {
        return dateViewed;
    }

    public void setDateViewed(Date dateViewed) {
        this.dateViewed = dateViewed;
    }
}
