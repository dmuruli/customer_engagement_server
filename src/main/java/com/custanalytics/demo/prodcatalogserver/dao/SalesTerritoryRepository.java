package com.custanalytics.demo.prodcatalogserver.dao;

import com.custanalytics.demo.prodcatalogserver.entity.SalesTerritory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesTerritoryRepository extends JpaRepository<SalesTerritory, Long> {

      SalesTerritory findOne(Long id);
}
