package com.custanalytics.demo.prodcatalogserver.dao.com.custanalytics.demo.prodcatalogserver;

import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository  extends JpaRepository<AppRole, Long> {

    @Query(value = "SELECT a FROM AppRole a WHERE a.deleted=false ")
    public List<AppRole> findAllActiveRoles();
}
