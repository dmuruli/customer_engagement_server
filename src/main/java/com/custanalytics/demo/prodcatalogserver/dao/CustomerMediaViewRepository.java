package com.custanalytics.demo.prodcatalogserver.dao;

import com.custanalytics.demo.prodcatalogserver.entity.CustomerMediaView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CustomerMediaViewRepository {

    @Query(value = "SELECT a FROM CustomerMediaView a WHERE a.userName = :username and a.password =:password ")
    public List<CustomerMediaView> getCustomerViewsBySalesPerson(@Param("salesPersonId") Long salesPersonId);

    @Query(value = "SELECT a FROM CustomerMediaView a WHERE a.userName = :username and a.password =:password ")
    public List<CustomerMediaView> getCustomerViewsBySalesPersonForDateRange(@Param("salesPersonId") Long salesPersonId,
                                                                             @Param("startDateRange") Date startDateRange,
                                                                             @Param("endDateRange") Date endDateRange);
}
