package com.custanalytics.demo.prodcatalogserver.dao;

import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository  extends JpaRepository<AppUser, Long> {

    List<AppUser>findUsersByLastName(String lastName);

    @Query(value = "SELECT a FROM AppUser a WHERE a.userName = :username and a.password =:password ")
    AppUser login(@Param("username") String userId, @Param("password") String password);

}
