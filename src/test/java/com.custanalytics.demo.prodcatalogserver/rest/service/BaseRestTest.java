package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by were on 12/28/2016.
 */
public class BaseRestTest {
    private MockMvc mockMvc;
    private long testRequestorId =-9999;
    @Autowired
    private WebApplicationContext webApplicationContext;

    public MockMvc getMockMvc() {
        return mockMvc;
    }

    public long getTestRequestorId() {
        return testRequestorId;
    }

    public WebApplicationContext getWebApplicationContext() {
        return webApplicationContext;
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}
