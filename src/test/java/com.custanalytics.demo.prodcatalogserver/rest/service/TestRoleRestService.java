package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.AppConfig;
import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import com.custanalytics.demo.prodcatalogserver.rest.resource.Request;
import com.custanalytics.demo.prodcatalogserver.service.RoleService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;


import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class TestRoleRestService {
    private MockMvc mockMvc;
    @Autowired
    RoleRestService roleRestService;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private RoleService roleService;
    private long testRequestorId =-9999;
    private AppRole testAppRole;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    private AppRole createTestRole() {
        AppRole testAppRole = new AppRole();
        roleService.saveRole(testRequestorId, testAppRole);
        return testAppRole;
    }


    @Test
    public void testRetrieveRole() throws Exception {
        mockMvc.perform(get("/" + "getAppRole/")
                .param("roleid", "1")
                .param("requestorid", "11")
        ).andDo(print())
        .andExpect(status().isOk());

    }
     public void testRetrieveSavedRole() throws Exception {

         String roleName ="Test_Save_Role_" + System.currentTimeMillis();
         mockMvc.perform(post("/" + "saveRole/")
                 .param("requestorid", String.valueOf(testRequestorId))
                 .requestAttr("roleName",roleName)
         ).andExpect(status().isOk());

     }

    @Test
    public void testSaveRole()throws Exception{
        AppRole testAppRole = new AppRole();
        String roleName ="TestSaveRole_" + System.currentTimeMillis();
        testAppRole.setRoleName(roleName);
        Request request = new Request();
        request.setRequestObject(testAppRole);

        mockMvc.perform(post("/" + "saveRole/")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(convertObjectToJsonBytes(request))
                ).andDo(print())
                .andExpect(status().isOk());

    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
}
