package com.custanalytics.demo.prodcatalogserver.rest.service;

import com.custanalytics.demo.prodcatalogserver.AppConfig;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import com.custanalytics.demo.prodcatalogserver.rest.resource.ApplicationRequest;
import com.custanalytics.demo.prodcatalogserver.rest.resource.Request;
import com.custanalytics.demo.prodcatalogserver.service.AppUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class TestAppUserRestService extends  BaseRestTest {
    @Autowired
    AppUserRestService appUserRestService;
    @Autowired
    AppUserService userService;
    Long testRoleId = 1L;
    Long testRequestorId =-9999L;

    @Test
    public void testGetUsers() throws Exception {
        getMockMvc().perform(get("/" + "getAppUsers/")
                .param("requestorid", "-9999")
        ).andDo(print())
         .andExpect(status().isOk())
         .andExpect(jsonPath("entity.[0].firstName").value("Janet"));

    }
    @Test
    public void testFindUser() throws Exception {
        getMockMvc().perform(get("/" + "findAppUser/")
                .param("requestorid", "-9999")
                .param("userId", "-9991")
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("entity.firstName").value("Joe"))
                .andExpect(jsonPath("entity.appRole.roleName").value("Sales"));

    }
    @Test
    public void testCreateUser() throws Exception {
        ApplicationRequest request = new ApplicationRequest();
        AppUser testAppUser = createTestAppUser("Test User");
        request.setAppUser(testAppUser);

        getMockMvc().perform(post("/" + "createAppUser/")
                .param("requestorId", "-9999")
                .param("roleId", "1")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(request))
        ).andDo(print())
                .andExpect(status().isOk());

    }

    private AppUser createTestAppUser(String userName) {
        AppUser testAppUser = new AppUser();
        testAppUser.setFirstName("First Name");
        testAppUser.setLastName("Last Name");
        testAppUser.setPassword("password");
        testAppUser.setUserName(userName + System.currentTimeMillis());
        return testAppUser;
    }

    @Test
    public void testUpdateUser() throws Exception {

        AppUser testAppUser = createTestAppUser("Update Test User");
        AppUser testUpdateUser = userService.createUser(testAppUser,testRequestorId);
        testUpdateUser.setUserName("New Updated UserName " + System.currentTimeMillis());
        ApplicationRequest request = new ApplicationRequest();
        request.setRequestorId(testRequestorId);
        request.setAppUser(testUpdateUser);

        getMockMvc().perform(post("/" + "updateAppUser/")
                .contentType(APPLICATION_JSON_UTF8)
                .content(convertObjectToJsonBytes(request))
        ).andDo(print())
                .andExpect(status().isOk());

    }

}
