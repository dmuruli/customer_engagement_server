package com.custanalytics.demo.prodcatalogserver.service;

import com.custanalytics.demo.prodcatalogserver.AppConfig;
import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import com.custanalytics.demo.prodcatalogserver.entity.AppUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
public class TestAppUserService {
    @Autowired
    AppUserService appUserService;
    @Autowired
    RoleService roleService;
    private long testRequestorId = -9999;
    private long ADMIN_ROLE_ID = 1;
    long testUserId = -9991;

    @Test
    public void testCreateUser() {
        AppUser newAppUser = createAppUser();
        AppUser createdAppUser = appUserService.createUser(newAppUser, ADMIN_ROLE_ID, testRequestorId);
        assertNotNull("created user is null", createdAppUser);
        assertNotNull("Created date is null", createdAppUser.getCreated());
    }

    private AppUser createAppUser() {
        AppUser appUser = new AppUser();
        appUser.setFirstName("Test First Name");
        appUser.setLastName("Test Last Name");
        appUser.setPassword("testpassword");
        appUser.setUserName("testusername");
        appUser.setStatus("D");
        return appUser;
    }

    @Test
    public void testUpdateUser() {
        AppUser appUser = appUserService.findUser(testUserId);
        String originalUserName = appUser.getUserName();

        String newUserName = "Test User Name " + System.currentTimeMillis();
        System.out.println("New user name: " + newUserName);
        appUser.setUserName(newUserName);
        appUser.setUpdatedBy(testRequestorId);
        appUserService.updateUser(appUser, testRequestorId);
        AppUser refereshedUser = appUserService.findUser(testUserId);
        assertTrue("Updated User name is not different from orginal user, updated failed",
                !originalUserName.equalsIgnoreCase(refereshedUser.getUserName()));

    }

    @Test
    public void testGetUserList() {
        List<AppUser> users = appUserService.getAllUsers();
        assertNotNull("users collection is null", users);
        assertTrue("user list size is not greater than zero", users.size() > 0);
    }

    @Test
    public void testFindUser() {
        AppUser appUser = appUserService.findUser(testUserId);
        assertNotNull("App User with id: " + testUserId + " is  null", appUser);
    }



}

