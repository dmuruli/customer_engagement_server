package com.custanalytics.demo.prodcatalogserver.service;

import com.custanalytics.demo.prodcatalogserver.AppConfig;
import com.custanalytics.demo.prodcatalogserver.entity.AppRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
public class TestRoleService {

    @Autowired
    RoleService roleService;
    @Autowired
    AppUserService appUserService;
    private long testRequestorId =-9999;

    @Test
    public void testCreateRole(){
        AppRole testAppRole =createRole();
        AppRole savedAppRole =roleService.saveRole(testRequestorId,testAppRole);
    }

    @Test
    public void testFindRole(){
        Long testRoleId = -9999L;
        AppRole testAppRole = roleService.findAppRole(testRoleId);
    }

    @Test
    public void testFindAllRoles(){
        List<AppRole> appRoles = roleService.listAllActiveRoles();

    }
    private AppRole createRole(){
        AppRole newAppRole = new AppRole();
        newAppRole.setRoleName("Test Role "+ System.currentTimeMillis() );
        return newAppRole;
    }
}
