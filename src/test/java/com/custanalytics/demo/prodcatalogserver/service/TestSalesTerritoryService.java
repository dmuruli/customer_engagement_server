package com.custanalytics.demo.prodcatalogserver.service;

import com.custanalytics.demo.prodcatalogserver.AppConfig;
import com.custanalytics.demo.prodcatalogserver.entity.SalesTerritory;
import com.custanalytics.demo.prodcatalogserver.rest.service.SaleTerritoryService;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppConfig.class)
@WebAppConfiguration
public class TestSalesTerritoryService {

    private MockMvc mockMvc;
    @Autowired
    SaleTerritoryService saleTerritoryService;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private long testRequestorId =-9999;
    private Long testSalesTerritoryId =-9999L;


    @Test
    public void testRetrieveAllSalesTerritories() throws Exception {

       List<SalesTerritory> salesTerritories= saleTerritoryService.getAllSalesTerritories(testRequestorId);
        assertTrue("did not find 1 or more sales Territories",salesTerritories.size()>0);

    }

    @Test
    public void testRetrieveOneSalesTerritory(){

        SalesTerritory salesTerritory = saleTerritoryService.findSalesTerritory(testSalesTerritoryId);
        assertNotNull("salesTerritory is null",salesTerritory);
        assertTrue("salesTerritory id is not " + testSalesTerritoryId,salesTerritory.getId()==testRequestorId);
    }

}
