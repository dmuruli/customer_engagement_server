INSERT INTO public.app_user(
            id, role_id, first_name, last_name, "password", user_name, created, created_by)
VALUES (-9999, 1, 'Janet', 'Admin', 'admin1', 'jadmin', CURRENT_TIMESTAMP, -99999);

INSERT INTO public.app_user(
            id, role_id, first_name, last_name, "password", user_name,created, created_by)
VALUES (-99999, 1, 'Super', 'Admin', 'admin1', 'sadmin', CURRENT_TIMESTAMP, -99999);

INSERT INTO public.app_user(
            id, role_id, first_name, last_name, "password", user_name, created, created_by)
VALUES (-9991, 2, 'Joe', 'Salesperson', 'jsales1', 'jsales', CURRENT_TIMESTAMP, -99999);

<AppUser>{user_id:-999,firstName:"Alvina", lastName:"Shanahan",  status:"Disabled",lastLoginIn:"01-07-2017", role:'doctor'};
        <AppUser>{user_id:-999,firstName:"Kristofer", lastName:"Crooks",  status:"Disabled",lastLoginIn:"01-07-2017", role:'doctor'};
        <AppUser>{user_id:-999,firstName:"Victor ", lastName:"Kirlin",  status:"ACTIVE", lastLoginIn:"01-08-2017",  role:'doctor'};
    	 <AppUser>{user_id:-999,firstName:"Myah", lastName:"Feeney",  status:"ACTIVE",lastLoginIn:"01-07-2017",  role:'doctor'};
    	<AppUser>{user_id:-999,firstName:"Jace", lastName:"Wolff",  status:"ACTIVE",lastLoginIn:"11-07-2016", role:'doctor'};
    	 <AppUser>{user_id:-999,firstName:"Malachi", lastName:"Kuphal",  status:"ACTIVE",lastLoginIn:"01-07-2017", role:'doctor'};
    	 <AppUser>{user_id:-999,firstName:"Ima", lastName:"Keebler",  status:"Disabled ",lastLoginIn:"01-08-2017",  role:'doctor'};
    
        let appUser8: AppUser = <AppUser>{user_id:-888,firstName:"Joseph",lastName:"Sams",status:"ACTIVE",email:"jsams@samsclubz.com",phone:"555-121-1212"};

!-- Regular Sample Data to demo --!
INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name, email, status,created, created_by)
VALUES (20, 1, 'Alvina', 'Shanahan', 'jsales1', 'jsales','ashnahan@customer.com','D' ,CURRENT_TIMESTAMP, -99999);
INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name, status,created, created_by)
VALUES (21, 2, 'Kristofer', 'Crooks', 'jsales2', 'jsales2','D',CURRENT_TIMESTAMP, -99999);

INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name,email, status,created, created_by)
VALUES (22, 2, 'Victor', 'Kirlin', 'jsales4', 'jsales4','vkirlin@customer2.com','D',CURRENT_TIMESTAMP, -99999);


INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name, status,created, created_by)
VALUES (24, 2, 'Myah', 'Feeney', 'jsales6', 'jsales6','A',CURRENT_TIMESTAMP, -99999);

INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name, status,created, created_by)
VALUES (25, 2, 'Malachi', 'Kuphal', 'jsales7', 'jsales7','A',CURRENT_TIMESTAMP, -99999);

INSERT INTO public.app_user(
            id, role_id, first_name, last_name, password, user_name, status,created, created_by)
VALUES (26, 2, 'Ima', 'Keebler', 'jsales8', 'jsales8','A',CURRENT_TIMESTAMP, -99999);




INSERT INTO public."role"(
            id, role_name, deleted, created, created_by)
VALUES (1, 'Admin', false,  CURRENT_TIMESTAMP, -9999);


INSERT INTO public."role"(
            id, role_name, deleted, created, created_by)
VALUES (2, 'Sales', false,  CURRENT_TIMESTAMP, -9999);